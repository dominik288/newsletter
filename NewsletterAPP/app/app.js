﻿(function(angular) {
    angular.module('newsletterApp', ['ngRoute', 'ngMaterial', 'ngMessages', 'ngResource'])
        .config(config);

    function config($routeProvider) {
        $routeProvider
            .when('/newsletter',
                {
                    templateUrl: './app/components/newsletter/newsletterForm.html',
                    controller: 'newsletterController'
                })
            .otherwise(
                {
                    redirectTo: '/newsletter'
                });
    }
}(window.angular));