﻿(function(angular) {
    'use strict';

    angular
        .module('newsletterApp')
        .factory('subscriptionResource', factory);

    function factory($resource) {
        const basePath = 'http://localhost:60530/api/subscriptions';

        function save(subscription) {
            return $resource(basePath).save(subscription).$promise;
        }

        return {
            save
        }
    }

}(window.angular));