﻿(function(angular) {
    'use strict';

    angular
        .module('newsletterApp')
        .factory('sourceResource', factory);

    function factory($resource) {
        const basePath = 'http://localhost:60530/api/sources';

        function getAll() {
            return $resource(basePath).query().$promise;
        }

        return {
            getAll
        }
    }

}(window.angular));