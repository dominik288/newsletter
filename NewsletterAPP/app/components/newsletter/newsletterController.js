﻿(function(angular) {
    'use strict';

    angular
        .module('newsletterApp')
        .controller('newsletterController', controller);

    function controller($scope, $mdToast, sourceResource, subscriptionResource) {
        function submit(isValid) {
            if (!isValid) {
                return;
            }

            $scope.loading = true;

            subscriptionResource.save($scope.subscription)
                .then(() => {
                    notify('Your email has been added to the database.');
                    refreshForm();
                })
                .catch(err => {
                    if (err.status === 400) {
                        notify(err.data.Message)
                    } else {
                        console.error('Error occured while singing in.');
                    }                    
                })
                .then(() => {
                    $scope.loading = false;
                });
        }

        function populateSources() {
            $scope.loading = true;
            sourceResource.getAll()
                .then(sources => {
                    $scope.loading = false;
                    $scope.sources = sources;
                })
                .catch(() => {
                    notify('Unable connect to the server.');
                });
        }

        function refreshForm() {
            $scope.subscription = {};
            $scope.newsletterForm.$setPristine();
            $scope.newsletterForm.$setUntouched();
        }

        function notify(message) {
            $mdToast.show(
                $mdToast.simple()
                  .textContent(message)
                  .position('top right')
                  .hideDelay(3000)
              );
        }

        $scope.submit = submit;
        $scope.subscription = {};
        populateSources();
    }
}(window.angular));