﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using NewsletterAPI.Models;
using NewsletterAPI.Peristance;
using NewsletterAPI.Shared;

namespace NewsletterAPI.Controllers
{
    public class SubscriptionsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public SubscriptionsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        [EnableCors("http://localhost:60534", "*", "*")]
        public IHttpActionResult Add(SubscriptionDto dto)
        {
            var validator = new EmailValidator();
            if (!validator.IsValidEmail(dto.Email))
            {
                return BadRequest("Incorrect email format.");
            }

            var emailExists = _unitOfWork.Subscriptions.GetByEmail(dto.Email) != null;

            if (emailExists)
            {
                return BadRequest("This email already exists.");
            }

            var sourceExists = _unitOfWork.Sources.GetById(dto.SourceId) != null;

            if (!sourceExists)
            {
                return BadRequest("Incorrect source identifier.");
            }

            var subscription = new Subscription(dto.Email, dto.SourceId, dto.Reason);

            _unitOfWork.Subscriptions.Add(subscription);
            _unitOfWork.Complete();

            return Ok();
        }
    }
}