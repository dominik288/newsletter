﻿using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using NewsletterAPI.Models;
using NewsletterAPI.Peristance;
using NewsletterAPI.Repositories;

namespace NewsletterAPI.Controllers
{
    [EnableCors("http://localhost:60534", "*", "*")]
    public class SourcesController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public SourcesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<Source> GetSources()
        {
            return _unitOfWork.Sources.GetSources();
        }
    }
}