namespace NewsletterAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UseEmailAsSubscriptionPrimaryKey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Subscriptions");
            AddPrimaryKey("dbo.Subscriptions", "Email");
            DropColumn("dbo.Subscriptions", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Subscriptions", "Id", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Subscriptions");
            AddPrimaryKey("dbo.Subscriptions", "Id");
        }
    }
}
