namespace NewsletterAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddValidationAndMissingFields : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Subscriptions", "Source_Id", "dbo.Sources");
            DropIndex("dbo.Subscriptions", new[] { "Source_Id" });
            AddColumn("dbo.Subscriptions", "CreatedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Sources", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Subscriptions", "Email", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Subscriptions", "Reason", c => c.String(maxLength: 255));
            AlterColumn("dbo.Subscriptions", "Source_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Subscriptions", "Source_Id");
            AddForeignKey("dbo.Subscriptions", "Source_Id", "dbo.Sources", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscriptions", "Source_Id", "dbo.Sources");
            DropIndex("dbo.Subscriptions", new[] { "Source_Id" });
            AlterColumn("dbo.Subscriptions", "Source_Id", c => c.Int());
            AlterColumn("dbo.Subscriptions", "Reason", c => c.String());
            AlterColumn("dbo.Subscriptions", "Email", c => c.String());
            AlterColumn("dbo.Sources", "Name", c => c.String());
            DropColumn("dbo.Subscriptions", "CreatedDate");
            CreateIndex("dbo.Subscriptions", "Source_Id");
            AddForeignKey("dbo.Subscriptions", "Source_Id", "dbo.Sources", "Id");
        }
    }
}
