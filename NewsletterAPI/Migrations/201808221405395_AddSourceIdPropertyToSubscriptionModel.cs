namespace NewsletterAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSourceIdPropertyToSubscriptionModel : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Subscriptions", name: "Source_Id", newName: "SourceId");
            RenameIndex(table: "dbo.Subscriptions", name: "IX_Source_Id", newName: "IX_SourceId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Subscriptions", name: "IX_SourceId", newName: "IX_Source_Id");
            RenameColumn(table: "dbo.Subscriptions", name: "SourceId", newName: "Source_Id");
        }
    }
}
