namespace NewsletterAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sources",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Reason = c.String(),
                        Source_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sources", t => t.Source_Id)
                .Index(t => t.Source_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscriptions", "Source_Id", "dbo.Sources");
            DropIndex("dbo.Subscriptions", new[] { "Source_Id" });
            DropTable("dbo.Subscriptions");
            DropTable("dbo.Sources");
        }
    }
}
