namespace NewsletterAPI.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class SeedSources : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO SOURCES (Name) VALUES ('Advert')");
            Sql("INSERT INTO SOURCES (Name) VALUES ('Word of Mouth')");
            Sql("INSERT INTO SOURCES (Name) VALUES ('Other')");
        }

        public override void Down()
        {
            Sql("DELETE FROM SOURCES WHERE ID = 1");
            Sql("DELETE FROM SOURCES WHERE ID = 2");
            Sql("DELETE FROM SOURCES WHERE ID = 3");
        }
    }
}