namespace NewsletterAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetStringLengthForSourceName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Sources", "Name", c => c.String(nullable: false, maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Sources", "Name", c => c.String(nullable: false));
        }
    }
}
