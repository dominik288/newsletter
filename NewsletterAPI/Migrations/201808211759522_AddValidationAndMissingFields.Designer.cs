// <auto-generated />
namespace NewsletterAPI.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddValidationAndMissingFields : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddValidationAndMissingFields));
        
        string IMigrationMetadata.Id
        {
            get { return "201808211759522_AddValidationAndMissingFields"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
