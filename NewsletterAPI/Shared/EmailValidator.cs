﻿using System.ComponentModel.DataAnnotations;

namespace NewsletterAPI.Shared
{
    public class EmailValidator
    {
        public bool IsValidEmail(string email)
        {
            return new EmailAddressAttribute().IsValid(email);
        }
    }
}