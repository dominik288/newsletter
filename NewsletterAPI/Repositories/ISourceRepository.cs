﻿using System.Linq;
using NewsletterAPI.Models;

namespace NewsletterAPI.Repositories
{
    public interface ISourceRepository
    {
        Source GetById(int id);
        IQueryable<Source> GetSources();
    }
}