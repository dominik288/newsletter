﻿using NewsletterAPI.Models;

namespace NewsletterAPI.Repositories
{
    public interface ISubscriptionRepository
    {
        void Add(Subscription subscription);
        Subscription GetByEmail(string email);
    }
}