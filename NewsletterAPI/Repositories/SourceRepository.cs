﻿using System.Linq;
using NewsletterAPI.Models;

namespace NewsletterAPI.Repositories
{
    public class SourceRepository : ISourceRepository
    {
        private readonly NewsletterApiContext _context;

        public SourceRepository(NewsletterApiContext context)
        {
            _context = context;
        }

        public IQueryable<Source> GetSources()
        {
            return _context.Sources;
        }

        public Source GetById(int id)
        {
            return _context.Sources.FirstOrDefault(s => s.Id == id);
        }
    }
}