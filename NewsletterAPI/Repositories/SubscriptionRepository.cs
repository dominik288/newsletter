﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NewsletterAPI.Models;

namespace NewsletterAPI.Repositories
{
    public class SubscriptionRepository : ISubscriptionRepository
    {
        private readonly NewsletterApiContext _context;

        public SubscriptionRepository(NewsletterApiContext context)
        {
            _context = context;
        }

        public void Add(Subscription subscription)
        {
            _context.Subscriptions.Add(subscription);
        }

        public Subscription GetByEmail(string email)
        {
            return _context.Subscriptions.SingleOrDefault(s => s.Email == email);
        }
    }
}