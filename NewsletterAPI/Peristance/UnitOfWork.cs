﻿using NewsletterAPI.Models;
using NewsletterAPI.Repositories;

namespace NewsletterAPI.Peristance
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly NewsletterApiContext _context;

        public ISourceRepository Sources { get; private set; }
        public ISubscriptionRepository Subscriptions { get; private set; }

        public UnitOfWork(NewsletterApiContext context)
        {
            _context = context;
            Sources = new SourceRepository(context);
            Subscriptions = new SubscriptionRepository(context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}