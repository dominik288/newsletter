﻿using NewsletterAPI.Repositories;

namespace NewsletterAPI.Peristance
{
    public interface IUnitOfWork
    {
        ISourceRepository Sources { get; }
        ISubscriptionRepository Subscriptions { get; }

        void Complete();
    }
}