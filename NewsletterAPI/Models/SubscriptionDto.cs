﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewsletterAPI.Models
{
    public class SubscriptionDto
    {
        public string Email { get; set; }
        public int SourceId { get; set; }
        public string Reason { get; set; }
    }
}