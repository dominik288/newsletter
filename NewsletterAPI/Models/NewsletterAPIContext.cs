﻿using System.Data.Entity;

namespace NewsletterAPI.Models
{
    public class NewsletterApiContext : DbContext
    {
        public NewsletterApiContext() : base("name=NewsletterAPIContext")
        {
        }

        public DbSet<Source> Sources { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
    }
}