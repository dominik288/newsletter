﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NewsletterAPI.Models
{
    public class Subscription
    {
        [Required]
        [StringLength(255)]
        [Key]
        public string Email { get; private set; }

        [Required]
        public int SourceId { get; private set; }

        public Source Source { get; private set; }

        [StringLength(255)]
        public string Reason { get; private set; }

        [Required]
        public DateTime CreatedDate { get; private set; }

        public Subscription(string email, int sourceId, string reason)
        {
            Email = email;
            SourceId = sourceId;
            Reason = reason;
            CreatedDate = DateTime.Now;
        }

        public Subscription()
        {
            
        }
    }
}