﻿using System;
using System.Web.Http.Results;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewsletterAPI.Controllers;
using NewsletterAPI.Models;
using NewsletterAPI.Peristance;
using NewsletterAPI.Repositories;

namespace NewsletterAPI.Tests.Controllers
{
    [TestClass]
    public class SubscriptionsControllerTests
    {
        private SubscriptionsController _controller;
        private Mock<ISourceRepository> _sourceRepository;
        private Mock<ISubscriptionRepository> _subscriptionRepository;

        public SubscriptionsControllerTests()
        {
            _subscriptionRepository = new Mock<ISubscriptionRepository>();
            _sourceRepository = new Mock<ISourceRepository>();

            var mockUoW = new Mock<IUnitOfWork>();
            mockUoW.SetupGet(u => u.Subscriptions).Returns(_subscriptionRepository.Object);
            mockUoW.SetupGet(u => u.Sources).Returns(_sourceRepository.Object);

            _controller = new SubscriptionsController(mockUoW.Object);
        }

        [TestMethod]
        public void Add_InvalidEmail_ShouldReturnBadRequest()
        {
            var dto = new SubscriptionDto
            {
                Email = "abcd",
                Reason = "reason",
                SourceId = 1
            };
            var result = _controller.Add(dto);

            result.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [TestMethod]
        public void Add_SourceWithGivenIdNotExists_ShouldReturnBadRequest()
        {
            _sourceRepository.Setup(r => r.GetById(1)).Returns(default(Source));

            var dto = new SubscriptionDto
            {
                Email = "abcd@ac.com",
                Reason = "reason",
                SourceId = 1
            };
            var result = _controller.Add(dto);

            result.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [TestMethod]
        public void Add_EmailAlreadyExists_ShouldReturnBadRequest()
        {
            _sourceRepository.Setup(r => r.GetById(1)).Returns(new Source {Id = 1, Name = "TestSource"});

            _subscriptionRepository.Setup(r => r.GetByEmail("test@test.com"))
                .Returns(new Subscription("test@test.com", 1, ""));

            var dto = new SubscriptionDto
            {
                Email = "test@test.com",
                Reason = "reason",
                SourceId = 1
            };
            var result = _controller.Add(dto);

            result.Should().BeOfType<BadRequestErrorMessageResult>();
        }

        [TestMethod]
        public void Add_EmailIsValidAndNotExistsAndSourceExists_ShouldReturnBadRequest()
        {
            _sourceRepository.Setup(r => r.GetById(1)).Returns(new Source { Id = 1, Name = "TestSource" });

            _subscriptionRepository.Setup(r => r.GetByEmail("test@test.com"))
                .Returns(default(Subscription));

            var dto = new SubscriptionDto
            {
                Email = "test@test.com",
                Reason = "reason",
                SourceId = 1
            };
            var result = _controller.Add(dto);

            result.Should().BeOfType<OkResult>();
        }
    }
}